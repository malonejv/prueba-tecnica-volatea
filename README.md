# Prueba técnica Volatea

###################################################
#######              TENNIS                 #######
###################################################

# Especifiaciones:

- Implementar un algoritmo que juegue un partido de tennis para dos jugadores.
  
  # Requerimientos:

- Crear una Api Rest para jugar un partido de tenis (partido a 3 ó 5 sets):

- Lógica:
  
  - Se haga un sorteo inicial para saber que jugador saca.
  - Se juegue el partido con las reglas de puntuación definidas en https://es.wikipedia.org/wiki/Tenis#Puntuaci.C3.B3n

- Guardar el partido en base de datos

- Respuesta:
  
  - Nombre de los jugadores
  
  - Sets
  
  - Detalle del juego    
    
    # Requerimientos técnicos:

- Visual Studio 2017 o superior (C#)

- .Net Core >= 2.0

- EntityFramework o cualquier ORM

- Hacer algunos unit test, no es necesario que tengo mucha cobertura de código.

- [Opcional] Uso de patrones de diseño, principios SOLID, inyección de dependencias, ... todo lo que se te ocurra! :)
  Partido de ejemplo:
    Pepo inicia el partido sacando
  
  * **Juego 1 - Set 1**    
      Punto de Pepo 15-0            (0-0, 0-0, 0-0)
      Punto de Pepo 30-0            (0-0, 0-0, 0-0)
      Punto de Supu 30-15           (0-0, 0-0, 0-0)
      Punto de Supu 30-30           (0-0, 0-0, 0-0)
      Punto de Pepo 40-30           (0-0, 0-0, 0-0)
      Punto de Supu 40-40           (0-0, 0-0, 0-0)
      Punto de Pepo Ad-40           (0-0, 0-0, 0-0)
      Pepo gana el juego            (1-0, 0-0, 0-0)
  
  * **Juego 2 - Set 1**
      Punto de Supu 0-15            (1-0, 0-0, 0-0)
      Punto de Supu 30-30           (1-0, 0-0, 0-0)
      Punto de Supu 30-40           (1-0, 0-0, 0-0)
      Supu gana el juego            (1-1, 0-0, 0-0)
  
  * ....
  
  * **Juego 8 - Set 1**
      Punto de Supu 0-15            (5-2, 0-0, 0-0)
      Punto de Supu 30-30           (5-2, 0-0, 0-0)
      Punto de Supu 30-40           (5-2, 0-0, 0-0)
      Supu gana el juego y el set   (6-2, 0-0, 0-0)
    
    ....
    
    !!PEPO GANA EL PARTIDO¡¡        (6-2, 6-7, 6-4)