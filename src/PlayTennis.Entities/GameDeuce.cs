﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace PlayTennis.Entities
{
    public class GameDeuce : Game
    {
        public GameDeuce(Match match, Set set, int game, List<GameScore> gameScore, List<Player> players) : base(match, set, game, gameScore, players)
        {
        }

        public override void UpdateGame()
        {
            if (CurrentPoint.Winner.Advantage)
            {
                this.Winner = CurrentPoint.Winner;
                this.Loser = CurrentPoint.Loser;
            }
            else if (CurrentPoint.Loser.Advantage)
            {
                CurrentPoint.Loser.Advantage = false;
            }
            else
            {
                CurrentPoint.Winner.Advantage = true;
            }
            AddDetail();
        }

    }
}
