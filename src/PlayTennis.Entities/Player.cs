﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PlayTennis.Entities
{
    public class Player
    {
        public Player()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public int CurrentSetScore { get; set; }
        public int CurrentGameScore { get; set; }
        public int CurrentGamePointsScore { get; set; }
        public bool Advantage { get; set; }
        public virtual bool Service { get; set; }

        [NotMapped]
        public Point CurrentPoint { get; set; }

        public Service Serve()
        {
            if (Service)
            {
                int count = Enum.GetValues<Service>().Length;
                Random random = new Random();
                int serviceState = random.Next(count);
                Service service = Enum.GetValues<Service>()[serviceState];

                if (service == Entities.Service.ValidService)
                {
                    Player player = CurrentPoint.Players.First(p => p != this);
                    CurrentPoint.CurrentPlayer = player;
                    CurrentPoint.OpponentPlayer = this;
                    player.CurrentPoint = CurrentPoint;
                }
                return service;
            }
            throw new Exception("Not server player");
        }

        public Hit Hit()
        {
            int count = Enum.GetValues<Hit>().Length;
            Random random = new Random();
            int hitState = random.Next(count);
            Hit hit = Enum.GetValues<Hit>()[hitState];

            if (hit == Entities.Hit.Hit)
            {
                Player currentOpponentPlayer = CurrentPoint.Players.First(p => p != this);
                currentOpponentPlayer.CurrentPoint = CurrentPoint;
                CurrentPoint.CurrentPlayer = currentOpponentPlayer;
                CurrentPoint.OpponentPlayer = this;
            }

            return hit;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
