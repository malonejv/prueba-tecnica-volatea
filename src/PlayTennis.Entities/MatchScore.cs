﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTennis.Entities
{
    public class MatchScore
    {
        public MatchScore()
        {
            Id = Guid.NewGuid();
            SetScore = new List<SetScore>();
        }

        public Guid Id { get; set; }
        public virtual List<SetScore> SetScore { get; set; }
    }
}
