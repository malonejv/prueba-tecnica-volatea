﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTennis.Entities
{
    public class GameScore
    {
        public GameScore() { }
        public GameScore(Player player)
        {
            Id = Guid.NewGuid();
            Service = player.Service;
            Player = player;
        }
        public Guid Id { get; set; }
        public bool Service { get; }
        public int Point { get; set; }
        public Player Player { get; }
    }
}
