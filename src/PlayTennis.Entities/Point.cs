﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayTennis.Common.Extensions;

namespace PlayTennis.Entities
{
    public class Point
    {
        public static int SERVICES_AVAILABLE = 2;
        private readonly Player serverPlayer;

        public Point() { }
        public Point(Match match, List<Player> players)
        {
            Id = Guid.NewGuid();
            Players = players;
            Match = match;
            serverPlayer = players.Where(p=>p.Service).First();
            serverPlayer.CurrentPoint = this;
            OpponentPlayer = Players.First(p => p != serverPlayer);
            CurrentPlayer = serverPlayer;
        }

        public Guid Id { get; set; }
        public virtual List<Player> Players { get; }
        public Match Match { get; }
        public Player CurrentPlayer { get; set; }
        public Player OpponentPlayer { get; set; }
        public Player Winner { get; private set; }
        public Player Loser { get; private set; }

        internal void Play()
        {
            int currentService = 1;
            Service service = Service.NotValidService;

            Match.AddDetail($"Service: { serverPlayer.Name }");

            while (currentService <= SERVICES_AVAILABLE && service == Service.NotValidService)
            {
                service = serverPlayer.Serve();

                string serviceResult = service == Service.NotValidService ? "bad service" : "good service";
                Match.AddDetail($"Service { currentService }: {  serviceResult }");
                currentService++;
            }
            Hit hit = Hit.LostPoint;
            if (service == Service.ValidService)
            {
                hit = Hit.Hit;
                while (hit == Hit.Hit)
                {
                    hit = CurrentPlayer.Hit();
                    if (hit == Hit.Hit)
                        Match.AddDetail($"{CurrentPlayer.Name} hits the ball");
                }
            }

            if (hit == Hit.LostPoint)
            {
                Winner = OpponentPlayer;
                Loser = CurrentPlayer;

                Match.AddDetail($"{Winner.Name} wins the point");

            }
        }
    }
}
