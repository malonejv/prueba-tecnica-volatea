﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace PlayTennis.Entities
{
    public class GamePoint : Game
    {
        public GamePoint(Match match, Set set, int game, List<GameScore> gameScore, List<Player> players) : base(match, set, game, gameScore, players)
        {
        }

        public override void UpdateGame()
        {
            int winnerPoints = GetScore(CurrentPoint.Winner);
            int loserPoints = GetScore(CurrentPoint.Loser);

            if (winnerPoints < MAX_POINT)
            {
                CurrentPoint.Winner.CurrentGamePointsScore++;
                winnerPoints = GetScore(CurrentPoint.Winner);
                AddDetail();
            }

            if (winnerPoints == MAX_POINT && loserPoints == MAX_POINT)
            {
                Set.Game = new GameDeuce(Match, Set, game, GameScore, players);
                Set.Game.Play();
                Winner = Set.Game.Winner;
            }
            else if (winnerPoints == MAX_POINT)
            {
                this.Winner = CurrentPoint.Winner;
                this.Loser = CurrentPoint.Loser;
            }
        }

    }
}
