﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PlayTennis.Entities
{
    public class Game
    {
        protected readonly int MAX_POINT = 40;

        protected int[] punctuation = { 0, 15, 30, 40 };
        protected int game;
        protected readonly List<Player> players;

        public Game() { }
        internal Game(Match match, Set set, int game, List<GameScore> gameScore, List<Player> players)
        {
            Id = Guid.NewGuid();
            this.Set = set;
            this.game = game;
            GameScore = gameScore;
            this.players = players;
            Match = match;
        }

        public Guid Id { get; set; }
        public virtual Match Match { get; }
        public virtual List<GameScore> GameScore { get; }
        [NotMapped]
        public Point CurrentPoint { get; set; }
        [NotMapped]
        public Player Winner { get; set; }
        [NotMapped]
        public Player Loser { get; set; }
        [NotMapped]
        public Set Set { get; set; }

        public virtual void Play()
        {
            while (Winner == null)
            {
                CurrentPoint = new Point(Match, players);
                Match.AddDetail($"Playing popint:");

                CurrentPoint.Play();
                UpdateGame();
            }
        }

        public virtual void UpdateGame()
        {
            int winnerPoints = GetScore(CurrentPoint.Winner);
            int loserPoints = GetScore(CurrentPoint.Loser);

            if (winnerPoints < MAX_POINT)
            {
                CurrentPoint.Winner.CurrentGamePointsScore++;
                winnerPoints = GetScore(CurrentPoint.Winner);
            }
            AddDetail();

            if (winnerPoints == MAX_POINT && loserPoints == MAX_POINT)
            {
                Set.Game = new GameDeuce(Match, Set, game, GameScore, players);
                Set.Game.Play();
                Winner = Set.Game.Winner;
                UpdateScore();
            }
            else if (winnerPoints == MAX_POINT)
            {
                Set.Game = new GamePoint(Match, Set, game, GameScore, players);
                Set.Game.Play();
                Winner = Set.Game.Winner;
                UpdateScore();
            }
        }

        public virtual int GetScore(Player player)
        {
            return punctuation[player.CurrentGamePointsScore];
        }

        public virtual string GetScoreDescription(Player player)
        {
            return player.Advantage ? "Ad" : punctuation[player.CurrentGamePointsScore].ToString();
        }

        public void UpdateScore()
        {
            GameScore gameScore = GameScore.Where(gs => gs.Player == Winner).FirstOrDefault();
            if (gameScore != null)
                gameScore.Point++;
        }

        protected virtual void AddDetail()
        {
            Player serverPlayer = Match.GetCurrentServerPlayer();
            Player opponnentPlayer = Match.GetCurrentOpponentPlayer();

            string serverPlayerDescription = $"{ serverPlayer.Name} (O)";
            string opponentPlayerDescription = $"{ opponnentPlayer.Name}";
            int padding = serverPlayerDescription.Length > opponentPlayerDescription.Length ? serverPlayerDescription.Length : opponentPlayerDescription.Length;
            Match.AddDetail($"{ serverPlayerDescription.PadRight(padding, ' ') }:  { serverPlayer.CurrentGameScore } | [{  GetScoreDescription(serverPlayer) }]");
            Match.AddDetail($"{ opponentPlayerDescription.PadRight(padding, ' ') }:  { opponnentPlayer.CurrentGameScore } | [{  GetScoreDescription(opponnentPlayer) }]");
        }


    }
}
