﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PlayTennis.Common.Extensions;

namespace PlayTennis.Entities
{
    public class Set
    {
        public static int GAMES_COUNT = 6;

        protected readonly List<Player> players;

        public Set() { }
        public Set(Match match, int set, List<SetScore> setScore, List<Player> players)
        {
            Id = Guid.NewGuid();
            Match = match;
            this.SetNum = set;
            SetScore = setScore;
            Players = players;
            this.players = players.ThrowIfNullOrEmpty();
        }

        public Guid Id { get; set; }
        public int SetNum { get; set; }
        public virtual List<Player> Players { get; }
        public virtual List<SetScore> SetScore { get; }
        public virtual Game Game { get; set; }
        public virtual Match Match { get; }
        [NotMapped]
        public Player Winner { get; set; }
        [NotMapped]
        public Player Loser { get; set; }

        public virtual void Play()
        {
            int game = 1;
            CreateNewGame(game);
            while (Winner == null)
            {
                Game.Play();
                UpdateSet();
                UpdateWinner(game);
            }
            UpdateScore();
        }

        private void UpdateScore()
        {
            SetScore setScore = SetScore.Where(gs => gs.Player == Winner).First();
            setScore.Point++;
        }

        protected void CreateNewGame(int game)
        {
            Match.AddDetail($"\nNew Game");
            int playerOneScore = players[0].CurrentGameScore;
            int playerTwoScore = players[1].CurrentGameScore;
            foreach (var player in players)
            {
                player.CurrentGamePointsScore = 0;
                player.Advantage = false;
            }

            List<GameScore> gameScore = SetScore.Select(s => s.GameScore).ToList();
            if (playerOneScore == GAMES_COUNT && playerTwoScore == GAMES_COUNT)
            {
                Game = new TieBreak(Match, this, game, gameScore, players);
            }
            else if (playerOneScore <= GAMES_COUNT)
            {
                Game = new Game(Match, this, game, gameScore, players);
            }
        }

        public virtual void UpdateSet()
        {
            int winnerGameScore = Game.Winner.CurrentGameScore;

            if (winnerGameScore < 6)
            {
                Game.Winner.CurrentGameScore++;
            }
            else if (winnerGameScore == 6 && Game.GetScore(Game.Winner) == 7)
            {
                Game.Winner.CurrentGameScore++;
            }
        }

        protected virtual void SwitchServer()
        {
            Player serverPlayer = players.Where(p => p.Service).First();
            Player opponentPlayer = players.Where(p => !p.Service).First();

            serverPlayer.Service = false;
            opponentPlayer.Service = true;
        }

        private void UpdateWinner(int game)
        {
            int playerOneScore = players[0].CurrentGameScore;
            int playerTwoScore = players[1].CurrentGameScore;

            if (playerOneScore > GAMES_COUNT || (playerOneScore == GAMES_COUNT && ((playerOneScore - playerTwoScore) >= 2)))
            {
                Winner = players[0];
            }
            else if (playerTwoScore > GAMES_COUNT || (playerTwoScore == GAMES_COUNT && ((playerTwoScore - playerOneScore) >= 2)))
            {
                Winner = players[1];
            }
            else
            {
                game++;
                CreateNewGame(game);
                SwitchServer();
            }
        }
    }
}
