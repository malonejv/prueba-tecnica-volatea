﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTennis.Entities
{
    public class SetScore
    {
        public SetScore() { }
        public SetScore(int set, Player player)
        {
            Id = Guid.NewGuid();
            SetNum = set;
            Player = player;
            GameScore = new GameScore(player);
        }

        public Guid Id { get; set; }
        public int SetNum { get; set; }
        public bool IsServer { get; }
        public virtual Player Player { get; set; }
        public virtual GameScore GameScore { get; set; }
        public int Point { get; set; }
    }
}
