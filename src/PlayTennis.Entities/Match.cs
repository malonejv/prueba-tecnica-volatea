﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.Extensions.Logging;
using PlayTennis.Common.Extensions;

namespace PlayTennis.Entities
{
    public class Match
    {
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger<Match> logger;

        public Match() { }
        public Match(int setsCount, List<Player> players, ILoggerFactory loggerFactory)
        {
            Id = Guid.NewGuid();
            SetsCount = setsCount;
            Players = players.ThrowIfNullOrEmpty();
            PlayedSets = new List<Set>();
            MatchScore = new MatchScore();
            MatchDetail = new List<MatchDetail>();

            this.loggerFactory = loggerFactory.ThrowIfNull();
            this.logger = loggerFactory.CreateLogger<Match>();
        }

        public Guid Id { get; set; }
        public int SetsCount { get; private set; }
        public virtual List<Player> Players { get; }
        public virtual MatchScore MatchScore { get; set; }
        public virtual List<MatchDetail> MatchDetail { get; }
        public virtual List<Set> PlayedSets { get; private set; }
        
        [NotMapped]
        public Player Winner { get; private set; }
        [NotMapped]
        public Set CurrentSet { get; private set; }


        public void FlipCoin()
        {
            AddDetail("Flip the coin");
            int count = Players.Count;
            Random random = new Random();
            int starter = random.Next(count);

            Players[starter].Service = true;
            AddDetail($"Starter player: {Players[starter].Name}");
        }

        public void Start()
        {
            int set = 1;
            Players.ForEach(p => p.CurrentSetScore = 0);

            while (Winner == null)
            {
                AddDetail($"Set: { set }\n" + $"------\n");
                InitScore(set);
                Players.ForEach(p => p.CurrentGameScore = 0);
                List<SetScore> sets = MatchScore.SetScore.Where(s => s.SetNum == set).ToList();
                CurrentSet = new Set(this, set, sets, Players);

                CurrentSet.Play();
                PlayedSets.Add(CurrentSet);
                UpdateWinner();
                UpdateMatchScore();
                set++;
            }
        }

        public virtual void UpdateMatch()
        {
            int winnerSetScore = CurrentSet.Winner.CurrentSetScore;

            if (winnerSetScore < SetsCount)
            {
                CurrentSet.Winner.CurrentSetScore++;
            }
        }

        internal Player GetCurrentServerPlayer()
        {
            return Players.Where(p => p.Service).First();
        }

        internal Player GetCurrentOpponentPlayer()
        {
            return Players.Where(p => !p.Service).First();
        }

        private void InitScore(int set)
        {
            Players.ForEach(p => MatchScore.SetScore.Add(new SetScore(set, p)));
        }

        private void UpdateMatchScore()
        {
            string matchDescription = GetMatchScoreDescription();
            AddDetail(matchDescription);
        }

        internal void AddDetail(string detail)
        {
            MatchDetail.Add(new MatchDetail(detail));
            logger.LogInformation(detail);
        }

        private void UpdateWinner()
        {
            if (PlayedSets.Count == SetsCount)
            {
                Winner = Players.Aggregate((p1, p2) => p1.CurrentSetScore > p2.CurrentSetScore ? p1 : p2);
            }
            else
            {
                foreach (var player in Players)
                {
                    int score = MatchScore.SetScore.Where(s => s.Player == player).Sum(s => s.Point);
                    if (score > (SetsCount / 2))
                    {
                        Winner = player;
                        break;
                    }
                }
            }
        }

        private string GetMatchScoreDescription()
        {
            Player serverPlayer = GetCurrentServerPlayer();
            Player opponnentPlayer = GetCurrentOpponentPlayer();

            string serverPlayerDescription = $"{ serverPlayer.Name} (O)";
            string opponentPlayerDescription = $"{ opponnentPlayer.Name}";
            int padding = serverPlayerDescription.Length > opponentPlayerDescription.Length ? serverPlayerDescription.Length : opponentPlayerDescription.Length;

            string setsDescription = "".PadLeft(padding + 1, ' ');
            string PlayerscoreDescription = "";
            foreach (var player in Players)
            {
                string playerDescription = player.Service ? serverPlayerDescription.PadRight(padding, ' ') : opponentPlayerDescription.PadRight(padding, ' ');
                PlayerscoreDescription += $"{ playerDescription }:";

                MatchScore.SetScore.Where(s => s.Player == player).ToList()
                    .ForEach(s => PlayerscoreDescription += $"   { s.GameScore.Point.ToString() }   |");
                PlayerscoreDescription += "\n";
            }
            MatchScore.SetScore.Select(s => s.SetNum).Distinct().ToList().ForEach(s => setsDescription += $" Set { s } |");

            return $"\n{setsDescription.PadLeft(padding, ' ')}\n{ PlayerscoreDescription }";
        }



    }
}
