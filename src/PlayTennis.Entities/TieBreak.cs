﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayTennis.Common.Extensions;

namespace PlayTennis.Entities
{
    public class TieBreak : Game
    {

        public TieBreak(Match match, Set set, int game, List<GameScore> gameScore, List<Player> players) : base(match, set, game, gameScore, players)
        {
        }

        public override void Play()
        {
            int services = 1;
            while (Winner == null)
            {
                CurrentPoint = new Point(Match, players);
                CurrentPoint.Play();
                services++;

                UpdateGame();
                if (services == 3)
                {
                    services = 1;
                    SwitchServer();
                }
            }
        }

        public override void UpdateGame()
        {
            int winnerPoints = CurrentPoint.Winner.CurrentGamePointsScore;
            int loserPoints = CurrentPoint.Loser.CurrentGamePointsScore;

            if (winnerPoints < 7 || (winnerPoints >= 7 && (winnerPoints - loserPoints) < 2))
            {
                CurrentPoint.Winner.CurrentGamePointsScore++;
                winnerPoints = CurrentPoint.Winner.CurrentGamePointsScore;
            }
            AddDetail();

            if (winnerPoints >= 7 && ((winnerPoints - loserPoints) >= 2))
            {
                this.Winner = CurrentPoint.Winner;
                this.Loser = CurrentPoint.Loser;
                UpdateScore();
            }
        }

        protected void SwitchServer()
        {
            Player serverPlayer = players.Where(p => p.Service).First();
            Player opponentPlayer = players.Where(p => !p.Service).First();

            serverPlayer.Service = false;
            opponentPlayer.Service = true;
        }

        public override int GetScore(Player player)
        {
            return player.CurrentGamePointsScore;
        }

        public override string GetScoreDescription(Player player)
        {
            return player.CurrentGamePointsScore.ToString();
        }
    }
}
