﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using PlayTennis.Common.Extensions;

namespace PlayTennis.Entities
{
    public class MatchDetail
    {
        public MatchDetail() { }
        public MatchDetail(string detail)
        {
            Id = Guid.NewGuid();
            Detail = detail.ThrowIfNullOrEmpty(nameof(Detail));
        }

        public Guid Id { get; set; }
        public string Detail { get; set; }
    }
}
