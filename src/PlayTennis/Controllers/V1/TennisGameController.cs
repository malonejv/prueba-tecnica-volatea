﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PlayTennis.Entities;
using PlayTennis.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PlayTennis.Services.Contracts;
using PlayTennis.ViewModels;
using System;
using PlayTennis.Common.Extensions;
using System.Linq;

namespace PlayTennis.Controllers
{
    [ApiController]
    [ApiVersionNeutral]
    [Route("[controller]")]
    public class TennisGameController : Controller
    {
        private readonly ITennisGameService tennisGameService;
        private readonly ILogger logger;

        public TennisGameController(ITennisGameService tennisGameService, ILogger<TennisGameController> logger)
        {
            this.tennisGameService = tennisGameService;
            this.logger = logger;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MatchViewModel))]
        public async Task<IActionResult> GetGameAsync(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));

            Match tennisGame = await tennisGameService.GetGameAsync(Guid.Parse(id));

            return Ok(ToMatchViewModel(tennisGame));
        }

        [HttpGet("{player1}/{player2}/{setsCount}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MatchViewModel))]
        public async Task<IActionResult> StartGameAsync(string player1, string player2, int setsCount)
        {
            PlayRequest playRequest = new PlayRequest()
            {
                Players = new List<Player>()
                {
                    new Player(){
                        Name = player1
                    },
                    new Player(){
                        Name = player2
                    }
                },
                SetsCount = setsCount
            };
            Match tennisGame = await tennisGameService.StartGameAsync(playRequest);

            return Ok(ToMatchViewModel(tennisGame));
        }

        private MatchViewModel ToMatchViewModel(Match match)
        {
            return new MatchViewModel()
            {
                MatchId = match.Id,
                Players = match.Players.Select(p => p.Name).ToList(),
                Score = ToScoreViewModel(match.MatchScore),
                Detail = match.MatchDetail.Select(d => d.Detail).ToList()
            };
        }

        private List<ScoreViewModel> ToScoreViewModel(MatchScore matchScore)
        {
            List<ScoreViewModel> result = new List<ScoreViewModel>();
            foreach (var setScore in matchScore.SetScore?.OrderBy(s => s.Player.Name).OrderBy(s => s.SetNum))
            {
                result.Add(
                    new ScoreViewModel()
                    {
                        Player = setScore.Player?.Name ?? "",
                        SetNum = setScore.SetNum,
                        GameScore = setScore.GameScore?.Point ?? 0
                    });
            };
            return result;
        }
    }
}
