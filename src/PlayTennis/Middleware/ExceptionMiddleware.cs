﻿using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using PlayTennis.Common.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace PlayTennis.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IHostEnvironment env;
        private readonly IConfiguration configuration;
        private readonly ILogger logger;

        public ExceptionMiddleware(RequestDelegate next, IHostEnvironment env, ILoggerFactory loggerFactory, IConfiguration configuration)
        {
            this.next = next;
            this.env = env;
            this.configuration = configuration;
            this.logger = loggerFactory.CreateLogger<ExceptionMiddleware>();
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception ex)
            {
                string errorId = Guid.NewGuid().ToString();
                logger.LogError(ex, $"ErrorID: { errorId } | ErrorDescription: { ex.Message }");
                await HandleExceptionAsync(httpContext, env, ex, errorId);
            }
        }

        private static Task HandleExceptionAsync(HttpContext httpContext, IHostEnvironment env, Exception ex, string errorId)
        {
            ApiError responseError;
            HttpStatusCode code = HttpStatusCode.InternalServerError;
            string message = $"ErrorID: { errorId } - { ex.Message }";

            if (ex is ArgumentException || ex is ArgumentNullException)
                code = HttpStatusCode.BadRequest;

            if (env.IsDevelopment())
            {
                responseError = new ApiError((int)code, message, ex.StackTrace.ToString());
            }
            else
            {
                responseError = new ApiError((int)code, message);
            }
            var jsonResponse = JsonSerializer.Serialize(responseError);

            httpContext.Response.StatusCode = (int)code;
            httpContext.Response.ContentType = "application/json";

            return httpContext.Response.WriteAsync(jsonResponse);
        }
    }
}
