﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayTennis.ViewModels
{
    public class ScoreViewModel
    {
        public string Player { get; set; }
        public int SetNum { get; set; }
        public int GameScore{ get; set; }

    }
}
