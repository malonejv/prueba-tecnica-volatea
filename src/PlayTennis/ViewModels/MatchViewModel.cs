﻿using PlayTennis.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayTennis.ViewModels
{
    public class MatchViewModel
    {
        public Guid MatchId { get; internal set; }
        public List<string> Players { get; set; }
        public List<ScoreViewModel> Score { get; set; }
        public List<string> Detail { get; set; }
    }
}
