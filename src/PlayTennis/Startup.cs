using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PlayTennis.Infraestructure.Repositories;
using PlayTennis.Infraestructure.Repositories;
using PlayTennis.Middleware;
using PlayTennis.Services.Services;

namespace PlayTennis
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<IContext, Context>();
            services.AddScoped<ITennisGameServiceValidator, TennisGameServiceValidator>();
            services.AddScoped<ITennisGameService, TennisGameService>();
            services.AddScoped<ITennisGameRepository, TennisGameRepository>();

            services.AddControllers();
            
            services.AddDbContext<Context>(options =>
                options
                .UseLazyLoadingProxies()
                .UseSqlite(Configuration.GetConnectionString("ContextDb")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TennisGame API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TextProcessorAPI v1"));
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
