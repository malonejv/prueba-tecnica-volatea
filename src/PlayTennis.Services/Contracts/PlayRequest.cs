﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayTennis.Entities;

namespace PlayTennis.Services.Contracts
{
    public class PlayRequest
    {
        public int SetsCount { get; set; }
        public List<Player> Players { get; set; }

    }
}
