﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PlayTennis.Common.Extensions;
using PlayTennis.Entities;
using PlayTennis.Infraestructure.Repositories;
using PlayTennis.Infraestructure.Repositories;
using PlayTennis.Services.Contracts;

namespace PlayTennis.Services.Services
{
    public class TennisGameService : ITennisGameService
    {
        private readonly ITennisGameRepository tennisGameRepository;
        private readonly IConfiguration configuration;
        private readonly ILogger<TennisGameService> logger;
        private readonly ILoggerFactory loggerFactory;
        private readonly ITennisGameServiceValidator validator;
        private readonly IContext context;

        public TennisGameService(
            ITennisGameServiceValidator validator,
            IContext context,
            ITennisGameRepository tennisGameRepository,
            ILogger<TennisGameService> logger,
            ILoggerFactory loggerFactory,
            IConfiguration configuration)
        {
            this.validator = validator.ThrowIfNull();
            this.tennisGameRepository = tennisGameRepository.ThrowIfNull();
            this.configuration = configuration.ThrowIfNull();
            this.logger = logger.ThrowIfNull();
            this.loggerFactory = loggerFactory.ThrowIfNull();
            this.context = context.ThrowIfNull();
        }

        public async Task<Match> GetGameAsync(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentException(nameof(id));

            return await tennisGameRepository.GetAsync(id);
        }

        public async Task<Match> StartGameAsync(PlayRequest playRequest)
        {
            playRequest.ThrowIfNull();

            if (playRequest.SetsCount == 0) playRequest.SetsCount = int.Parse(configuration["Rules:DefaultSetsCount"]);
            
            validator.ValidatePlayers(playRequest.Players);
            validator.ValidateSets(playRequest.SetsCount);

            logger.LogInformation("Tennis match started");

            Match match = new Match(playRequest.SetsCount, playRequest.Players, loggerFactory);
            match.FlipCoin();
            match.Start();
            match.UpdateMatch();

            logger.LogInformation("Tennis match ended");

            await tennisGameRepository.PersistAsync(match);
            await context.SaveChangesAsync();


            return match;
        }

    }
}
