﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayTennis.Common.Extensions;
using PlayTennis.Entities;

namespace PlayTennis.Services.Services
{
    public class TennisGameServiceValidator : ITennisGameServiceValidator
    {
        public void ValidatePlayers(List<Player> players)
        {
            players.ThrowIfNullOrEmpty();
        }

        public void ValidateSets(int setsCount)
        {
            if (setsCount != 3 && setsCount != 5)
                throw new ArgumentException("Sets must be 3 or 5", nameof(setsCount));
        }
    }
}
