﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayTennis.Entities;

namespace PlayTennis.Services.Services
{
    public interface ITennisGameServiceValidator
    {
        void ValidateSets(int setsCount);
        void ValidatePlayers(List<Player> players);
    }
}
