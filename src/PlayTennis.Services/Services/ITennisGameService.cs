﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PlayTennis.Entities;
using PlayTennis.Services.Contracts;

namespace PlayTennis.Services.Services
{
    public interface ITennisGameService
    {
        Task<Match> GetGameAsync(Guid id);
        Task<Match> StartGameAsync(PlayRequest playRequest);
    }
}
