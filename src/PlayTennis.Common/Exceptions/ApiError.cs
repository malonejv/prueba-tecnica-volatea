﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTennis.Common.Exceptions
{
    public class ApiError
    {
        public ApiError(int code, string message, string details = null)
        {
            Code = code;
            Message = message;
            Details = details;
        }

        public int Code { get; set; }
        public string Message { get; }
        public string Details { get; set; }
    }
}
