﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTennis.Common.Extensions
{
    public static class StringExtensions
	{
		/// <summary>
		/// The ThrowIfNullOrEmpty.
		/// </summary>
		/// <param name="text">The text<see cref="string"/>.</param>
		/// <param name="paramName">The paramName<see cref="string"/>.</param>
		public static string ThrowIfNullOrEmpty(this string text, string paramName)
		{
			if (string.IsNullOrEmpty(text))
			{
				throw new ArgumentNullException(paramName, $"Parameter {paramName} cannot be null.");
			}
			return text;
		}

	}
}
