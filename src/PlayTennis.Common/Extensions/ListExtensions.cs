﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PlayTennis.Common.Extensions
{
    public static class ListExtensions
    {

        /// <summary>
        /// The ThrowIfNullOrEmpty.
        /// </summary>
        /// <typeparam name="T">Type to treat.</typeparam>
        /// <param name="instance">The instance of type <c>T</c>.</param>
        public static List<T> ThrowIfNullOrEmpty<T>(this List<T> instance)
        {
            return ThrowIfNullOrEmpty<T>(instance, nameof(instance));
        }

        /// <summary>
        /// The ThrowIfNullOrEmpty.
        /// </summary>
        /// <typeparam name="T">Type to treat.</typeparam>
        /// <param name="instance">The instance of type <c>T</c>.</param>
        /// <param name="paramName">The paramName <see cref="string"/>.</param>
        public static List<T> ThrowIfNullOrEmpty<T>(this List<T> instance, string paramName)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(paramName, $"Parameter {paramName} cannot be null.");
            }
            if (instance.Count() == 0)
            {
                throw new ArgumentException(paramName, $"Parameter {paramName} cannot be empty.");
            }
            return instance;
        }

        /// <summary>
        /// Validates wether the instances is of type defined with <typeparamref name="TCheckType">TCheckType</typeparamref>.
        /// If it corresponds returns the same instances, in other case returns ArgumentException.
        /// </summary>
        /// <typeparam name="T">Type to treat in the validation.</typeparam>
        /// <typeparam name="TCheckType">Type used to check if parameter <paramref name="instance">instance</paramref> is of this type.</typeparam>
        /// <param name="instance">The instance of type <c>T</c>.</param>
        public static TCheckType ThrowIfNotOfType<T, TCheckType>(this T instance)
        {
            return ThrowIfNotOfType<T, TCheckType>(instance, nameof(instance));
        }

        /// <summary>
        /// Validates wether the instances is of type defined with <typeparamref name="TCheckType">TCheckType</typeparamref>.
        /// If it corresponds returns the same instances, in other case returns ArgumentException.
        /// </summary>
        /// <typeparam name="T">Type to treat in the validation.</typeparam>
        /// <typeparam name="TCheckType">Type used to check if parameter <paramref name="instance">instance</paramref> is of this type.</typeparam>
        /// <param name="instance">The instance of type <c>T</c>.</param>
        /// <param name="paramName">The paramName <see cref="string"/>.</param>
        public static TCheckType ThrowIfNotOfType<T, TCheckType>(this T instance, string paramName)
        {
            if (!(instance is TCheckType))
            {
                throw new ArgumentException(paramName, $"Parameter {paramName} cannot be null.");
            }
            return (TCheckType)Convert.ChangeType(instance, typeof(TCheckType));
        }
    }
}
