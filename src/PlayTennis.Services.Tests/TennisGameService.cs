using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using PlayTennis.Entities;
using PlayTennis.Infraestructure.Repositories;
using PlayTennis.Services.Contracts;
using PlayTennis.Services.Services;

namespace PlayTennis.Services.Tests
{
    public class TennisGameServiceTests
    {
        [Test]
        public void TennisGameServiceConstructor_WhenValidatorIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new TennisGameService(null, null, null, null, null, null));
        }

        [Test]
        public void TennisGameServiceConstructor_WhenContextIsNull_ThrowsArgumentNullException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();

            Assert.Throws<ArgumentNullException>(() => new TennisGameService(tennisGameServiceValidatorMock.Object, null, null, null, null, null));
        }

        [Test]
        public void TennisGameServiceConstructor_WhenTennisGameRepositoryIsNull_ThrowsArgumentNullException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();

            Assert.Throws<ArgumentNullException>(() => new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, null, null, null, null));
        }

        [Test]
        public void TennisGameServiceConstructor_WhenLoggerIsNull_ThrowsArgumentNullException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();

            Assert.Throws<ArgumentNullException>(() => new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, null, null, null));
        }

        [Test]
        public void TennisGameServiceConstructor_WhenLoggerFactoryIsNull_ThrowsArgumentNullException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();
            Mock<ILogger<TennisGameService>> loggerMock = new Mock<ILogger<TennisGameService>>();

            Assert.Throws<ArgumentNullException>(() => new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, loggerMock.Object, null, null));
        }

        [Test]
        public void TennisGameServiceConstructor_WhenConfigurationIsNull_ThrowsArgumentNullException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();
            Mock<ILogger<TennisGameService>> loggerMock = new Mock<ILogger<TennisGameService>>();
            Mock<ILoggerFactory> loggerFactoryMock = new Mock<ILoggerFactory>();

            Assert.Throws<ArgumentNullException>(() => new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, loggerMock.Object, loggerFactoryMock.Object, null));
        }

        [Test]
        public void TennisGameServiceConstructor_WhenValidParameters_CreatesInstance()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();
            Mock<ILogger<TennisGameService>> loggerMock = new Mock<ILogger<TennisGameService>>();
            Mock<ILoggerFactory> loggerFactoryMock = new Mock<ILoggerFactory>();
            Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();

            object result = new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, loggerMock.Object, loggerFactoryMock.Object, configurationMock.Object);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<TennisGameService>(result);
        }

        [Test]
        public void GetGameAsync_WhenIdIsEmpty_ThrowsArgumentException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();
            Mock<ILogger<TennisGameService>> loggerMock = new Mock<ILogger<TennisGameService>>();
            Mock<ILoggerFactory> loggerFactoryMock = new Mock<ILoggerFactory>();
            Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();

            TennisGameService classUnderTest = new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, loggerMock.Object, loggerFactoryMock.Object, configurationMock.Object);

            Assert.ThrowsAsync<ArgumentException>(() => classUnderTest.GetGameAsync(It.IsAny<Guid>()));
        }

        [Test]
        public void GetGameAsync_CallsTennisGameRepository_GetAsyncOnce()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();
            Mock<ILogger<TennisGameService>> loggerMock = new Mock<ILogger<TennisGameService>>();
            Mock<ILoggerFactory> loggerFactoryMock = new Mock<ILoggerFactory>();
            Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();
            Guid guidFake = Guid.NewGuid();

            TennisGameService classUnderTest = new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, loggerMock.Object, loggerFactoryMock.Object, configurationMock.Object);
            object result = classUnderTest.GetGameAsync(guidFake);

            tennisGameRepositoryMock.Verify(m => m.GetAsync(guidFake), Times.Once);
        }

        [Test]
        public void StartGameAsync_WhenPlayRequestIsNull_ThrowsArgumentNullException()
        {
            Mock<ITennisGameServiceValidator> tennisGameServiceValidatorMock = new Mock<ITennisGameServiceValidator>();
            Mock<IContext> contextMock = new Mock<IContext>();
            Mock<ITennisGameRepository> tennisGameRepositoryMock = new Mock<ITennisGameRepository>();
            Mock<ILogger<TennisGameService>> loggerMock = new Mock<ILogger<TennisGameService>>();
            Mock<ILoggerFactory> loggerFactoryMock = new Mock<ILoggerFactory>();
            Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();

            TennisGameService classUnderTest = new TennisGameService(tennisGameServiceValidatorMock.Object, contextMock.Object, tennisGameRepositoryMock.Object, loggerMock.Object, loggerFactoryMock.Object, configurationMock.Object);

            Assert.ThrowsAsync<ArgumentNullException>(() => classUnderTest.StartGameAsync(It.IsAny<PlayRequest>()));
        }

    }
}