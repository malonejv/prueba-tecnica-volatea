﻿using Microsoft.EntityFrameworkCore;
using PlayTennis.Entities;

namespace PlayTennis.Infraestructure.Repositories
{
    public class Context : DbContext, IContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<Match> Matches { get; set; }
        public DbSet<Player> Players { get; set; }
    }
}
