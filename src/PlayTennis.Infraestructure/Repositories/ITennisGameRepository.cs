﻿using System;
using System.Threading.Tasks;
using PlayTennis.Entities;

namespace PlayTennis.Infraestructure.Repositories
{
    public interface ITennisGameRepository 
    {
        Task<Match> GetAsync(Guid id);
        Task PersistAsync(Match game);
    }
}
