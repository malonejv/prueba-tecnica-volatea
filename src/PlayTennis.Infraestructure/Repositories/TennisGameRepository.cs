﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PlayTennis.Entities;

namespace PlayTennis.Infraestructure.Repositories
{
    public class TennisGameRepository : ITennisGameRepository
    {
        private readonly Context context;

        public TennisGameRepository(Context context)
        {
            this.context = context;
        }

        public async Task<Match> GetAsync(Guid id)
        {
            return await context.Matches
                                    .Include(m => m.Players)
                                    .Include(m => m.MatchScore.SetScore)
                                    .FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task PersistAsync(Match game)
        {
            await context.Matches.AddAsync(game);
        }
    }
}
