﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PlayTennis.Infraestructure.Repositories;

namespace PlayTennis.Infraestructure.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20220321014147_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.10");

            modelBuilder.Entity("PlayTennis.Entities.Game", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("CurrentPointId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("LoserId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("WinnerId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("CurrentPointId");

                    b.HasIndex("LoserId");

                    b.HasIndex("WinnerId");

                    b.ToTable("Game");
                });

            modelBuilder.Entity("PlayTennis.Entities.GameScore", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("GameId")
                        .HasColumnType("TEXT");

                    b.Property<int>("Point")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.ToTable("GameScore");
                });

            modelBuilder.Entity("PlayTennis.Entities.Match", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<int>("SetsCount")
                        .HasColumnType("INTEGER");

                    b.Property<Guid?>("WinnerId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("WinnerId");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("PlayTennis.Entities.MatchDetail", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Detail")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("MatchId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("MatchId");

                    b.ToTable("MatchDetail");
                });

            modelBuilder.Entity("PlayTennis.Entities.Player", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<bool>("Advantage")
                        .HasColumnType("INTEGER");

                    b.Property<int>("CurrentGamePointsScore")
                        .HasColumnType("INTEGER");

                    b.Property<int>("CurrentGameScore")
                        .HasColumnType("INTEGER");

                    b.Property<int>("CurrentSetScore")
                        .HasColumnType("INTEGER");

                    b.Property<Guid?>("MatchId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("PointId")
                        .HasColumnType("TEXT");

                    b.Property<bool>("Service")
                        .HasColumnType("INTEGER");

                    b.Property<Guid?>("SetId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("MatchId");

                    b.HasIndex("PointId");

                    b.HasIndex("SetId");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("PlayTennis.Entities.Point", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("CurrentPlayerId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("LoserId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("OpponentPlayerId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("WinnerId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("CurrentPlayerId");

                    b.HasIndex("LoserId");

                    b.HasIndex("OpponentPlayerId");

                    b.HasIndex("WinnerId");

                    b.ToTable("Point");
                });

            modelBuilder.Entity("PlayTennis.Entities.Set", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("GameId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("LoserId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("MatchId")
                        .HasColumnType("TEXT");

                    b.Property<int>("SetNum")
                        .HasColumnType("INTEGER");

                    b.Property<Guid?>("WinnerId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("LoserId");

                    b.HasIndex("MatchId");

                    b.HasIndex("WinnerId");

                    b.ToTable("Set");
                });

            modelBuilder.Entity("PlayTennis.Entities.SetScore", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<int>("Point")
                        .HasColumnType("INTEGER");

                    b.Property<Guid?>("SetId")
                        .HasColumnType("TEXT");

                    b.Property<int>("SetNum")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("SetId");

                    b.ToTable("SetScore");
                });

            modelBuilder.Entity("PlayTennis.Entities.Game", b =>
                {
                    b.HasOne("PlayTennis.Entities.Point", "CurrentPoint")
                        .WithMany()
                        .HasForeignKey("CurrentPointId");

                    b.HasOne("PlayTennis.Entities.Player", "Loser")
                        .WithMany()
                        .HasForeignKey("LoserId");

                    b.HasOne("PlayTennis.Entities.Player", "Winner")
                        .WithMany()
                        .HasForeignKey("WinnerId");

                    b.Navigation("CurrentPoint");

                    b.Navigation("Loser");

                    b.Navigation("Winner");
                });

            modelBuilder.Entity("PlayTennis.Entities.GameScore", b =>
                {
                    b.HasOne("PlayTennis.Entities.Game", null)
                        .WithMany("GameScore")
                        .HasForeignKey("GameId");
                });

            modelBuilder.Entity("PlayTennis.Entities.Match", b =>
                {
                    b.HasOne("PlayTennis.Entities.Player", "Winner")
                        .WithMany()
                        .HasForeignKey("WinnerId");

                    b.Navigation("Winner");
                });

            modelBuilder.Entity("PlayTennis.Entities.MatchDetail", b =>
                {
                    b.HasOne("PlayTennis.Entities.Match", null)
                        .WithMany("MatchDetail")
                        .HasForeignKey("MatchId");
                });

            modelBuilder.Entity("PlayTennis.Entities.Player", b =>
                {
                    b.HasOne("PlayTennis.Entities.Match", null)
                        .WithMany("Players")
                        .HasForeignKey("MatchId");

                    b.HasOne("PlayTennis.Entities.Point", null)
                        .WithMany("Players")
                        .HasForeignKey("PointId");

                    b.HasOne("PlayTennis.Entities.Set", null)
                        .WithMany("Players")
                        .HasForeignKey("SetId");
                });

            modelBuilder.Entity("PlayTennis.Entities.Point", b =>
                {
                    b.HasOne("PlayTennis.Entities.Player", "CurrentPlayer")
                        .WithMany()
                        .HasForeignKey("CurrentPlayerId");

                    b.HasOne("PlayTennis.Entities.Player", "Loser")
                        .WithMany()
                        .HasForeignKey("LoserId");

                    b.HasOne("PlayTennis.Entities.Player", "OpponentPlayer")
                        .WithMany()
                        .HasForeignKey("OpponentPlayerId");

                    b.HasOne("PlayTennis.Entities.Player", "Winner")
                        .WithMany()
                        .HasForeignKey("WinnerId");

                    b.Navigation("CurrentPlayer");

                    b.Navigation("Loser");

                    b.Navigation("OpponentPlayer");

                    b.Navigation("Winner");
                });

            modelBuilder.Entity("PlayTennis.Entities.Set", b =>
                {
                    b.HasOne("PlayTennis.Entities.Game", "Game")
                        .WithMany()
                        .HasForeignKey("GameId");

                    b.HasOne("PlayTennis.Entities.Player", "Loser")
                        .WithMany()
                        .HasForeignKey("LoserId");

                    b.HasOne("PlayTennis.Entities.Match", null)
                        .WithMany("PlayedSets")
                        .HasForeignKey("MatchId");

                    b.HasOne("PlayTennis.Entities.Player", "Winner")
                        .WithMany()
                        .HasForeignKey("WinnerId");

                    b.Navigation("Game");

                    b.Navigation("Loser");

                    b.Navigation("Winner");
                });

            modelBuilder.Entity("PlayTennis.Entities.SetScore", b =>
                {
                    b.HasOne("PlayTennis.Entities.Set", null)
                        .WithMany("SetScore")
                        .HasForeignKey("SetId");
                });

            modelBuilder.Entity("PlayTennis.Entities.Game", b =>
                {
                    b.Navigation("GameScore");
                });

            modelBuilder.Entity("PlayTennis.Entities.Match", b =>
                {
                    b.Navigation("MatchDetail");

                    b.Navigation("PlayedSets");

                    b.Navigation("Players");
                });

            modelBuilder.Entity("PlayTennis.Entities.Point", b =>
                {
                    b.Navigation("Players");
                });

            modelBuilder.Entity("PlayTennis.Entities.Set", b =>
                {
                    b.Navigation("Players");

                    b.Navigation("SetScore");
                });
#pragma warning restore 612, 618
        }
    }
}
