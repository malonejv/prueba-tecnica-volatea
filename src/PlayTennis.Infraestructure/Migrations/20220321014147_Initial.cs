﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayTennis.Infraestructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GameScore",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Point = table.Column<int>(type: "INTEGER", nullable: false),
                    GameId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameScore", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Set",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    SetNum = table.Column<int>(type: "INTEGER", nullable: false),
                    GameId = table.Column<Guid>(type: "TEXT", nullable: true),
                    WinnerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    LoserId = table.Column<Guid>(type: "TEXT", nullable: true),
                    MatchId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Set", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SetScore",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    SetNum = table.Column<int>(type: "INTEGER", nullable: false),
                    Point = table.Column<int>(type: "INTEGER", nullable: false),
                    SetId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetScore", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SetScore_Set_SetId",
                        column: x => x.SetId,
                        principalTable: "Set",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MatchDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Detail = table.Column<string>(type: "TEXT", nullable: true),
                    MatchId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    CurrentSetScore = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentGameScore = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentGamePointsScore = table.Column<int>(type: "INTEGER", nullable: false),
                    Advantage = table.Column<bool>(type: "INTEGER", nullable: false),
                    Service = table.Column<bool>(type: "INTEGER", nullable: false),
                    MatchId = table.Column<Guid>(type: "TEXT", nullable: true),
                    PointId = table.Column<Guid>(type: "TEXT", nullable: true),
                    SetId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Players_Set_SetId",
                        column: x => x.SetId,
                        principalTable: "Set",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    SetsCount = table.Column<int>(type: "INTEGER", nullable: false),
                    WinnerId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Matches_Players_WinnerId",
                        column: x => x.WinnerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Point",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    CurrentPlayerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    OpponentPlayerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    WinnerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    LoserId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Point", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Point_Players_CurrentPlayerId",
                        column: x => x.CurrentPlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Point_Players_LoserId",
                        column: x => x.LoserId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Point_Players_OpponentPlayerId",
                        column: x => x.OpponentPlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Point_Players_WinnerId",
                        column: x => x.WinnerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Game",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    WinnerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    LoserId = table.Column<Guid>(type: "TEXT", nullable: true),
                    CurrentPointId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Game", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Game_Players_LoserId",
                        column: x => x.LoserId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Game_Players_WinnerId",
                        column: x => x.WinnerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Game_Point_CurrentPointId",
                        column: x => x.CurrentPointId,
                        principalTable: "Point",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Game_CurrentPointId",
                table: "Game",
                column: "CurrentPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Game_LoserId",
                table: "Game",
                column: "LoserId");

            migrationBuilder.CreateIndex(
                name: "IX_Game_WinnerId",
                table: "Game",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_GameScore_GameId",
                table: "GameScore",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchDetail_MatchId",
                table: "MatchDetail",
                column: "MatchId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_WinnerId",
                table: "Matches",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_MatchId",
                table: "Players",
                column: "MatchId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_PointId",
                table: "Players",
                column: "PointId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_SetId",
                table: "Players",
                column: "SetId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_CurrentPlayerId",
                table: "Point",
                column: "CurrentPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_LoserId",
                table: "Point",
                column: "LoserId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_OpponentPlayerId",
                table: "Point",
                column: "OpponentPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_WinnerId",
                table: "Point",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_GameId",
                table: "Set",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_LoserId",
                table: "Set",
                column: "LoserId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_MatchId",
                table: "Set",
                column: "MatchId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_WinnerId",
                table: "Set",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_SetScore_SetId",
                table: "SetScore",
                column: "SetId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameScore_Game_GameId",
                table: "GameScore",
                column: "GameId",
                principalTable: "Game",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Game_GameId",
                table: "Set",
                column: "GameId",
                principalTable: "Game",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Matches_MatchId",
                table: "Set",
                column: "MatchId",
                principalTable: "Matches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Players_LoserId",
                table: "Set",
                column: "LoserId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Players_WinnerId",
                table: "Set",
                column: "WinnerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchDetail_Matches_MatchId",
                table: "MatchDetail",
                column: "MatchId",
                principalTable: "Matches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Players_Matches_MatchId",
                table: "Players",
                column: "MatchId",
                principalTable: "Matches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Players_Point_PointId",
                table: "Players",
                column: "PointId",
                principalTable: "Point",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Game_Players_LoserId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_Game_Players_WinnerId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_Matches_Players_WinnerId",
                table: "Matches");

            migrationBuilder.DropForeignKey(
                name: "FK_Point_Players_CurrentPlayerId",
                table: "Point");

            migrationBuilder.DropForeignKey(
                name: "FK_Point_Players_LoserId",
                table: "Point");

            migrationBuilder.DropForeignKey(
                name: "FK_Point_Players_OpponentPlayerId",
                table: "Point");

            migrationBuilder.DropForeignKey(
                name: "FK_Point_Players_WinnerId",
                table: "Point");

            migrationBuilder.DropForeignKey(
                name: "FK_Set_Players_LoserId",
                table: "Set");

            migrationBuilder.DropForeignKey(
                name: "FK_Set_Players_WinnerId",
                table: "Set");

            migrationBuilder.DropTable(
                name: "GameScore");

            migrationBuilder.DropTable(
                name: "MatchDetail");

            migrationBuilder.DropTable(
                name: "SetScore");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Set");

            migrationBuilder.DropTable(
                name: "Game");

            migrationBuilder.DropTable(
                name: "Matches");

            migrationBuilder.DropTable(
                name: "Point");
        }
    }
}
