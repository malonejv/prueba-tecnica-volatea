﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayTennis.Infraestructure.Migrations
{
    public partial class Change3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "MatchScoreId",
                table: "SetScore",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "MatchScoreId",
                table: "Matches",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MatchScore",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchScore", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SetScore_MatchScoreId",
                table: "SetScore",
                column: "MatchScoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_MatchScoreId",
                table: "Matches",
                column: "MatchScoreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_MatchScore_MatchScoreId",
                table: "Matches",
                column: "MatchScoreId",
                principalTable: "MatchScore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SetScore_MatchScore_MatchScoreId",
                table: "SetScore",
                column: "MatchScoreId",
                principalTable: "MatchScore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matches_MatchScore_MatchScoreId",
                table: "Matches");

            migrationBuilder.DropForeignKey(
                name: "FK_SetScore_MatchScore_MatchScoreId",
                table: "SetScore");

            migrationBuilder.DropTable(
                name: "MatchScore");

            migrationBuilder.DropIndex(
                name: "IX_SetScore_MatchScoreId",
                table: "SetScore");

            migrationBuilder.DropIndex(
                name: "IX_Matches_MatchScoreId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "MatchScoreId",
                table: "SetScore");

            migrationBuilder.DropColumn(
                name: "MatchScoreId",
                table: "Matches");
        }
    }
}
