﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayTennis.Infraestructure.Migrations
{
    public partial class Change4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "GameScoreId",
                table: "SetScore",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PlayerId",
                table: "SetScore",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SetScore_GameScoreId",
                table: "SetScore",
                column: "GameScoreId");

            migrationBuilder.CreateIndex(
                name: "IX_SetScore_PlayerId",
                table: "SetScore",
                column: "PlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_SetScore_GameScore_GameScoreId",
                table: "SetScore",
                column: "GameScoreId",
                principalTable: "GameScore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SetScore_Players_PlayerId",
                table: "SetScore",
                column: "PlayerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SetScore_GameScore_GameScoreId",
                table: "SetScore");

            migrationBuilder.DropForeignKey(
                name: "FK_SetScore_Players_PlayerId",
                table: "SetScore");

            migrationBuilder.DropIndex(
                name: "IX_SetScore_GameScoreId",
                table: "SetScore");

            migrationBuilder.DropIndex(
                name: "IX_SetScore_PlayerId",
                table: "SetScore");

            migrationBuilder.DropColumn(
                name: "GameScoreId",
                table: "SetScore");

            migrationBuilder.DropColumn(
                name: "PlayerId",
                table: "SetScore");
        }
    }
}
