﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlayTennis.Infraestructure.Migrations
{
    public partial class Change1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Game_Players_LoserId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_Game_Players_WinnerId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_Game_Point_CurrentPointId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_Players_Point_PointId",
                table: "Players");

            migrationBuilder.DropForeignKey(
                name: "FK_Set_Players_LoserId",
                table: "Set");

            migrationBuilder.DropForeignKey(
                name: "FK_Set_Players_WinnerId",
                table: "Set");

            migrationBuilder.DropTable(
                name: "Point");

            migrationBuilder.DropIndex(
                name: "IX_Set_LoserId",
                table: "Set");

            migrationBuilder.DropIndex(
                name: "IX_Set_WinnerId",
                table: "Set");

            migrationBuilder.DropIndex(
                name: "IX_Players_PointId",
                table: "Players");

            migrationBuilder.DropIndex(
                name: "IX_Game_CurrentPointId",
                table: "Game");

            migrationBuilder.DropIndex(
                name: "IX_Game_LoserId",
                table: "Game");

            migrationBuilder.DropIndex(
                name: "IX_Game_WinnerId",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "LoserId",
                table: "Set");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "Set");

            migrationBuilder.DropColumn(
                name: "PointId",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "CurrentPointId",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "LoserId",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "Game");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "LoserId",
                table: "Set",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "WinnerId",
                table: "Set",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PointId",
                table: "Players",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CurrentPointId",
                table: "Game",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LoserId",
                table: "Game",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "WinnerId",
                table: "Game",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Point",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    CurrentPlayerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    LoserId = table.Column<Guid>(type: "TEXT", nullable: true),
                    OpponentPlayerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    WinnerId = table.Column<Guid>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Point", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Point_Players_CurrentPlayerId",
                        column: x => x.CurrentPlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Point_Players_LoserId",
                        column: x => x.LoserId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Point_Players_OpponentPlayerId",
                        column: x => x.OpponentPlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Point_Players_WinnerId",
                        column: x => x.WinnerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Set_LoserId",
                table: "Set",
                column: "LoserId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_WinnerId",
                table: "Set",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_PointId",
                table: "Players",
                column: "PointId");

            migrationBuilder.CreateIndex(
                name: "IX_Game_CurrentPointId",
                table: "Game",
                column: "CurrentPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Game_LoserId",
                table: "Game",
                column: "LoserId");

            migrationBuilder.CreateIndex(
                name: "IX_Game_WinnerId",
                table: "Game",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_CurrentPlayerId",
                table: "Point",
                column: "CurrentPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_LoserId",
                table: "Point",
                column: "LoserId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_OpponentPlayerId",
                table: "Point",
                column: "OpponentPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Point_WinnerId",
                table: "Point",
                column: "WinnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Game_Players_LoserId",
                table: "Game",
                column: "LoserId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Game_Players_WinnerId",
                table: "Game",
                column: "WinnerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Game_Point_CurrentPointId",
                table: "Game",
                column: "CurrentPointId",
                principalTable: "Point",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Players_Point_PointId",
                table: "Players",
                column: "PointId",
                principalTable: "Point",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Players_LoserId",
                table: "Set",
                column: "LoserId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Players_WinnerId",
                table: "Set",
                column: "WinnerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
